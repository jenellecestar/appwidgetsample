package com.example.macstudent.appwidgetsample;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

import java.text.DateFormat;
import java.util.Date;


/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {


    public static final String PREFERENCES_NAME = "AppWidgetSamplePreferences";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // PUT ALL LOGIC FOR CONTROLLING THE WIDGET IN HERE


        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);

        //-------------------------------------------
        // UI nonsense -> update your textviews


        String time = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());
        views.setTextViewText(R.id.appwidget_text2, time);


        //how many times have we loaded our widget?

        // 1. GET THE COUNT FROM SP
        //----------------------------------
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        // create a count variable
        int count = prefs.getInt("loadCounter", 0);

        // each time you load the widget, increase the counter
        count = count + 1;


        // 2. UPDATE THE WIDGET UI TO SHOW THE COUNT
        //----------------------------------
        // show the new count in the widget
        views.setTextViewText(R.id.appwidget_text, "Count? " + count);


        // 3. SAVE THE UPDATED COUNT BACK TO SHARED PREFERENCES
        //----------------------------------
        // save the count to shared preferences
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putInt("loadCounter",count);
        prefEditor.apply();



        // EVERYTIME YOU PUSH THE BUTTON,
        // RUN THIS FUNCTION AGAIN! THAT"S IT!
        Intent intentUpdate = new Intent(context, NewAppWidget.class);
        intentUpdate.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);

        int[] idArray = new int[]{appWidgetId};
        intentUpdate.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, idArray);

        PendingIntent pendingUpdate = PendingIntent.getBroadcast(
                context, appWidgetId, intentUpdate,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // specifcially, the button click handler is here
        views.setOnClickPendingIntent(R.id.button1, pendingUpdate);




        //-------------------------------------------
        // DO NOT REMOVE THIS! Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        // YOU DON"T HAVE TO DO ANYTHING HERE
        // JUST LEAVE IT!

        // --> THIS FUNCTION GETS RUN WHEN YOU LOAD YOUR WIDGET

        // onCreate() -> mainactivity

        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

